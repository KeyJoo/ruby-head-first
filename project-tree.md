```
.
├── book
│   ├── en
│   │   └── book-content.md
│   ├── index.html
│   └── ru
│       ├── book-content.ru.md
│       └── chapters
│           ├── chapter-01
│           │   ├── chap-01.txt
│           │   ├── programming_usefull.md
│           │   └── programming_usefull.ru.md
│           ├── chapter-02
│           │   └── chap-02.txt
│           └── chapter-03
│               └── chapter-03.ru.md
├── examples
│   ├── ex.rb
│   └── methods_and_arguments.rb
├── exercises
│   └── dog.rb
├── get_number.rb
├── index-get-number.sh
├── lib
│   └── lib.rb
├── project-tree.md
├── README.ru.md
├── test
│   └── test.rb
└── tree

```

11 directories, 18 files
